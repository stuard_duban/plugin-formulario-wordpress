<?php

class Report 
{
	public function __construct() {

	}

	public function generateReport() {
		global $wpdb;


		$encuestas = $wpdb->prefix . "encuesta_astronautas";
		$motivo_luna = $wpdb->prefix . "motivo_luna";
		$otro_motivo_luna = $wpdb->prefix . "otro_motivo_luna";
		$contacto = $wpdb->prefix . "contacto_extraterrestre";

		

		$sql_reporte = "SELECT $encuestas.id,
                  $encuestas.id,
                 $encuestas.nombre_completo,
                 $encuestas.edad,
                 $encuestas.sexo,
	              $encuestas.correo_electronico,
	                         $motivo_luna.id,
	                                      $motivo_luna.motivo,
	                                                   $contacto.tiempo,
			   $otro_motivo_luna.motivo as otro_motivo
			FROM $encuestas			     
			     LEFT JOIN $motivo_luna ON $encuestas.id_motivo_luna = $motivo_luna.id
			     LEFT JOIN $contacto ON $encuestas.id_contacto_extraterrestre = $contacto.id
			 LEFT JOIN $otro_motivo_luna ON $otro_motivo_luna.id_encu = $encuestas.id
			WHERE true ";
		
		

		$result = $wpdb->get_results($sql_reporte);

		$this->viewReport($result);
	}

	/*
	* Mostrar en el admin el listado del formulario
	*/
	public function viewReport($result) {

		require_once plugin_dir_path(__FILE__) . '../templates/reports.php';
		
	}
}