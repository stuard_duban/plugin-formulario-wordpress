<?php
//require($_SERVER['DOCUMENT_ROOT'].'/GradiWeb/Nasa/wp-load.php');
require(__DIR__.'/../../../../wp-load.php');

class Configuration
{
	private $rutaLogo;

	public function __construct(){
	   require(__DIR__.'/../../../../wp-load.php');
	}
	

	public function generateConfiguration() {
		global $wpdb;


		$descripcion = $wpdb->prefix . "datos_descripciones";

		

		$sql_descripcion = "SELECT * FROM $descripcion";
		
		

		$result = $wpdb->get_results($sql_descripcion);

		$this->viewConfiguration($result);
	}

	public function viewConfiguration($result) {

		require_once plugin_dir_path(__FILE__) . '../templates/configurations.php';
		
	}

	public function saveData($datos) {
		global $wpdb;

		$this->rutaLogo = plugin_dir_path(__FILE__) . '../images/';

		
		//print_r($_FILES["logo"]["name"]);
		//$target_dir = plugin_dir_path(__FILE__) . '../images/';

		$target_file = $this->rutaLogo . basename($_FILES["logo"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["logo"]["tmp_name"]);
		    if($check !== false) {
		        //echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        //echo "File is not an image.";
		        $uploadOk = 0;
		    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    //echo "El archivo ya existe";
		    $uploadOk = 0;
		}
		// Check file size
		if ($_FILES["fileToUpload"]["size"] > 500000) {
		    //echo "El archivo es muy grande";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    //echo "Solo se permite los formatos JPG, JPEG, PNG & GIF";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    //echo "Tu archivo no fue subido";
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
		        //echo "El archivo ". basename( $_FILES["logo"]["name"]). " fue subido.";
		        $datos['logo'] = plugin_dir_url("") . "NASA_astronautas/images/" .basename( $_FILES["logo"]["name"]);
		    } else {
		        //echo "Hubó un error subiendo el archivo";
		    }
		}

		//echo plugin_dir_url("") . "NASA_astronautas/images/" .basename( $_FILES["logo"]["name"]);

		//echo "Directorio $target_dir";exit;

		$table_name = $wpdb->prefix . 'datos_descripciones';

		$sql_descripcion = "UPDATE $table_name SET ";

		if($datos['logo'] != "") {
			$sql_descripcion .= "logo = '" . $datos['logo'] . "' ";
		}
		if($datos['descripciones'] != "") {
			$sql_descripcion .= ", descripciones = '" . $datos['descripciones'] . "' ";
		}

		$sql_descripcion .= " WHERE id=1";

		//echo $sql_descripcion;

		return $result = $wpdb->query($sql_descripcion);
	}
}




if(count($_POST) != 0) {
	//var_dump($_POST);
	$config = new Configuration();

	if($config->saveData($_POST) != NULL) {
		header("Location: ".get_site_url()."/wp-admin/admin.php?page=configuracion-nasa-astronautas&act=true");
	}

	//$config->saveData($_POST);

	//header("Location: ".get_site_url()."/wp-admin/admin.php?page=configuracion-nasa-astronautas");

	exit;	
}
//echo "Location: ".get_site_url()."/wp-admin/admin.php?page=configuracion-nasa-astronautas";


//header("Location: ".get_site_url()."/wp-admin/admin.php?page=configuracion-nasa-astronautas");
