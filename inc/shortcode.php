<?php

class Shortcode {
/**
 * Shortcodes del plugin
 */
	public function formulario () {
		global $wpdb;

		$motivo_luna = $wpdb->prefix . "motivo_luna";
		
		$contacto = $wpdb->prefix . "contacto_extraterrestre";

		$sql_motivo = "SELECT *	FROM $motivo_luna";

		$motivos = $wpdb->get_results($sql_motivo);
		

		$sql_contacto = "SELECT * FROM $contacto";

		$contactos = $wpdb->get_results($sql_contacto);

		$dataForm = $this->dataForm();

		$this->viewFormulario(array($motivos, $contactos, $dataForm));

	}

	public function viewFormulario($result) {

		require_once plugin_dir_path(__FILE__) . '../templates/formulario.php';
		
	}

	/*
	* Funcion para obtener los datos(imagenes, textos) para mostrar en el formulario
	*/

	public function dataForm() {
		global $wpdb;
		
		$descripcion = $wpdb->prefix . "datos_descripciones";

		$sql_descripcion = "SELECT *	FROM $descripcion";

		$descripcion = $wpdb->get_results($sql_descripcion);

		return $descripcion;
		
	}
}