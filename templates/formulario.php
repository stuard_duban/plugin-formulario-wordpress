<?php
//echo "<pre>";
//var_dump($result);
//Cambiar por algo mas dinamico
require(__DIR__.'/../../../../wp-load.php');

$urlAction = plugin_dir_url("NASA_astronautas") . 'NASA_astronautas/inc/formulario.php';


?>
<div class="register">
    <div class="row">
      <div class="col-md-12 contenedor-logo"><img src="<?php echo $result[2][0]->logo ?>"></div>
      <div class="col-md-12">
        <h3 class="register-heading"><?php echo $result[2][0]->descripciones ?></h3>
          <div class="row register-form">
          <div class="col-md-3">
          </div>
          <div class="col-md-6">
            <div class="col-md-12">
              <form method="post" action="<?php echo($urlAction) ?>">
                <div class="form-group">
                  <label for="name">Nombre:</label>
                  <input type="text" id="name" name="nombre" class="form-control" placeholder="Nombre Completo" value="" required="true"/>
                </div>
                <div class="form-group">
                  <label for="edad">Edad:</label>
                  <input type="number" id="edad" name="edad" class="form-control" placeholder="Su Edad" value="" required="true"/>
                </div>
                <div class="form-group">
                  <label for="sexo">Sexo:</label>
                  <input type="radio" name="hm" value="0"> Hombre
                  <input type="radio" name="hm" value="1"> Mujer
                </div>
                <div class="form-group">
                  <label for="correo">Correo Electrónico:</label>
                  <input type="email" name="correo" class="form-control" id="mail" placeholder="Su correo" required="true"/>
                </div>
                <div class="form-group">
                  <label for="motivo">Motivo para ir a la luna</label>
                  <select class="form-control"  name="motivo" required="true">
                  	<option selected="selected">
					Indique una opción
					</option>
                  <?php
                  	$motivos = array_reverse ($result[0]);
                  	$string = "";
                  	foreach ($motivos as $key => $motivo) {
                  		$string .= "<option value='".$motivo->id."'>".$motivo->motivo."</option>";
                  	}
                  	echo $string;
                  ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="contacto">Última vez que tuvo contacto con Extraterrestre</label>
                    <select class="form-control" name="contacto" required="true">
                    <option selected="selected">
					Indique una opción
					</option>
	                  <?php
	                  	//$motivos = array_reverse ($result[0]);
	                  	$string = "";
	                  	foreach ($result[1] as $key => $contacto) {
	                  		$string .= "<option value='".$contacto->id."'>".$contacto->tiempo."</option>";
	                  	}
	                  	echo $string;
	                  ?>
                  </select>
                </div>
                <input type="submit" name="Button" class="btnContactSubmit" value="Enviar" />
              </form>
              
            </div>
          </div>
          <div class="col-md-3">
          </div>
        </div>
      </div>
    </div>
  </div>