<?php
/**
* @package NASA_astronautas
*/
/**
 * Plugin Name: NASA Astronautas
 * Plugin URI:        http://localhost/GradiWeb/Nasa/wp-admin/plugins/NASA_astronautas/
 * Description:       Formulario de la Nasa para la recoleccion de datos de Astronautas
 * Version:           1.0.0
 * Requires PHP:      7.3.4
 * Author:            Stuard Romero
 * License:			  GPLv2 or later
 * Text Domain:       NASA_astronautas
 */

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2005-2015 Automattic, Inc.
*/

if ( !defined( 'ABSPATH' ) ) {
	die;
}



	class NasaAstronautas
	{
		public function __construct() {
			//add_action( 'init', array( $this, 'custom_post_type') );
			//add_action( 'init', array( $this, 'custom_post_type') );
			add_action( 'admin_menu', array( $this, 'itemMenu') );

			//Custom post type
			//add_action( 'init', array($this, 'custom_post_type') );

			//add_filter( 'template_include', array($this, 'rc_tc_template_chooser'));



			add_shortcode('vistaformulario', array( $this, 'vistaForm'));
		}

		private function createTables() {
			global $wpdb;

			# Tabla de encuestas
			$table_name = $wpdb->prefix . 'encuesta_astronautas';
			

			$sql_encuesta = "CREATE TABLE IF NOT EXISTS $table_name ( `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT , `nombre_completo` VARCHAR(255) NOT NULL , `edad` INT UNSIGNED NOT NULL , `sexo` BOOLEAN NOT NULL COMMENT '1 para mujer, 0 para hombre' , `correo_electronico` VARCHAR(255) NOT NULL , `id_motivo_luna` INT UNSIGNED NOT NULL , `id_contacto_extraterrestre` INT UNSIGNED NOT NULL , PRIMARY KEY (`id`),
				FOREIGN KEY(id_contacto_extraterrestre) REFERENCES `".$wpdb->prefix."contacto_extraterrestre` (`id`),
				FOREIGN KEY(id_motivo_luna) REFERENCES `".$wpdb->prefix."motivo_luna` (`id`)
				) ENGINE = InnoDB; ";

			$table_name = $wpdb->prefix . 'motivo_luna';

			$sql_motivo = "CREATE TABLE IF NOT EXISTS $table_name ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `motivo` VARCHAR(1000) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
				INSERT INTO $table_name (`id`, `motivo`) VALUES
				(1, 'Otro'),
				(2, 'Descubrir un ser que jamas existió'),
				(3, 'Ordeñar vacas lunares');";

			$table_name = $wpdb->prefix . 'otro_motivo_luna';

			$sql_otro_motivo = "CREATE TABLE IF NOT EXISTS $table_name ( `id_encu` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT , `motivo` VARCHAR(1000) NOT NULL , PRIMARY KEY (`id_encu`),
				FOREIGN KEY(id_encu) REFERENCES `".$wpdb->prefix."encuesta_astronautas` (`id`)
			) ENGINE = InnoDB; ";


			$table_name = $wpdb->prefix . 'contacto_extraterrestre';

			$sql_contacto = "CREATE TABLE IF NOT EXISTS $table_name ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `tiempo` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;
				INSERT INTO $table_name (`id`, `tiempo`) VALUES
				(1, '0-6 Meses'),
				(2, '6 meses - 1 año');
				";

			//Tabla para guardar descripciones y logo


			$table_name = $wpdb->prefix . 'datos_descripciones';

			$sql_datos_descripciones = "CREATE TABLE IF NOT EXISTS $table_name ( `id` INT UNSIGNED NULL, `logo` VARCHAR(1000) NULL , `descripciones` VARCHAR(2000) NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;

			INSERT INTO $table_name (id) values(1)";


			//echo $sql_encuesta .  $sql_motivo . $sql_contacto;exit;

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			
			
			 
			dbDelta( $sql_motivo );
			dbDelta( $sql_contacto );
			
			dbDelta( $sql_encuesta );

			dbDelta( $sql_otro_motivo );
			//Stephania

			dbDelta( $sql_datos_descripciones );

			//exit;

			

		}

		public function activate() {
			$this->createTables();

			//Para actvar el shortcode cuando active el plguin
			//$this->funcionShortcode();

			
			//$this->custom_post_type();
			flush_rewrite_rules();
		}

		public function deactivate() {
			flush_rewrite_rules();
		}


		public function itemMenu() {
			//register_post_type('nasa_clientes', ['public' => true, 'label' => 'NASA clientes']);
			$page_title = 'NASA astronautas';
			$menu_title = 'NASA astronautas';
			$capability = 'manage_options';
			$menu_slug_parent  = 'nasa-astronautas';
			$functionReport   = array($this, 'showReport');				//Funcion que mostrara el contenido
			$icon_url   = 'dashicons-media-code';
			$position   = 4;

			add_menu_page( $page_title, $menu_title, $capability, $menu_slug_parent, $functionReport, $icon_url, $position );

			$functionConfiguration = array($this, 'showConfiguration');

			add_submenu_page( $menu_slug_parent, 'Reporte Formulario', 'Reportes', 'manage_options', 'reporte-nasa-astronautas', $functionReport);

			add_submenu_page( $menu_slug_parent, 'Configuracion Formulario', 'Configuración', 'manage_options', 'configuracion-nasa-astronautas', $functionConfiguration);
			/*add_submenu_page( 'theme-options', 'FAQ page title', 'FAQ menu label', 'manage_options', 'theme-op-faq', 'wps_theme_func_faq');
				
			*/
		}

		public function showReport() {

			require_once plugin_dir_path(__FILE__) . 'inc/report.php';


			$reportEncuesta = new Report();

			$reportEncuesta->generateReport();

		}

		public function showConfiguration() {

			require_once plugin_dir_path(__FILE__) . 'inc/configuration.php';


			$config = new Configuration();

			$config->generateConfiguration();


 
				
		}



		/*
		* enqueueAdmin
		* Funcion para encolar assets en el admin
		*/
		public function enqueueAdmin() {
			//Encolar estilos para el admin
			wp_enqueue_style('admin_styles', plugins_url('/assets/admin-styles.css', __FILE__ ));
		}

		public function enqueueFront() {
			//Encolar estilos para el admin
			wp_enqueue_style('styles_nasa', plugins_url('/assets/styles-nasa.css', __FILE__ ));

			//ecolar estilos
			wp_enqueue_style('bootstrap-min', plugins_url('/assets/bootstrap.min.css', __FILE__ ));
		}

		public function registerAssets() {
			add_action('admin_enqueue_scripts', array($this, 'enqueueAdmin'));

			add_action('wp_enqueue_scripts', array($this, 'enqueueFront'), 50);
		}

		/*
		* Stephania Mi bebe
		*/
		
		
		public function vistaForm() {

			

			require_once plugin_dir_path(__FILE__) . 'inc/shortcode.php';

			$formulario = new Shortcode();
			
			$formulario->formulario();
			
			
			}
			
		



	}

	

if( class_exists('NasaAstronautas') ) {
	$nasaAtronautas = new NasaAstronautas();

	$nasaAtronautas->registerAssets();
}

//Activación del plugin
register_activation_hook( __FILE__, array($nasaAtronautas, 'activate'));

//Desactivación del plugin
register_deactivation_hook( __FILE__, array($nasaAtronautas, 'deactivate'));

//Desinstalación del plugin
//register_uninstall_hook(    __FILE__, 'uninstall' );

//register_uninstall_hook($this->Path_Get('plugin_file_index'), 'Uninstall');

